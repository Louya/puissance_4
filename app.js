 const colAct = document.querySelectorAll('.colact');
//  Déclarations de variables globales
const moves = document.querySelector(".moves");
const myReset = document.querySelector('#reset');
const zoneGris1 = document.querySelector('.zoneGris1');
const zoneGris2 = document.querySelector('.zoneGris2');
let color;
let colorActive;
let compteur;
let playerActive;
let x;
let y;
let i = 0;


// Initialisation du jeu au chargement de la page
window.addEventListener('DOMContentLoaded', function init() {
    playerActive = 1;
    compteur = 42;
    tourJoueur();
    //  Ecoute du click sur les cases d'entrées----------------------------------------------------------------
    const colonne1 = document.querySelector("#colonne1");
    const colonne2 = document.querySelector("#colonne2");
    const colonne3 = document.querySelector("#colonne3");
    const colonne4 = document.querySelector("#colonne4");
    const colonne5 = document.querySelector("#colonne5");
    const colonne6 = document.querySelector("#colonne6");
    const colonne7 = document.querySelector("#colonne7");


    colonne1.addEventListener("click", function () {
        if (colonne1 === colonne1) {
            x = 0;
            place_jeton();
        }
    });

    colonne2.addEventListener("click", function () {
        if (colonne2 === colonne2) {
            x = 1;
            place_jeton();
        }
    });

    colonne3.addEventListener("click", function () {
        if (colonne3 === colonne3) {
            x = 2;
            place_jeton();
        }
    });

    colonne4.addEventListener("click", function () {
        if (colonne4 === colonne4) {
            x = 3;
            place_jeton();
        }
    });

    colonne5.addEventListener("click", function () {
        if (colonne5 === colonne5) {
            x = 4;
            place_jeton();
        }
    });

    colonne6.addEventListener("click", function () {
        if (colonne6 === colonne6) {
            x = 5;
            place_jeton();
        }
    });

    colonne7.addEventListener("click", function () {
        if (colonne7 === colonne7) {
            x = 6;
            place_jeton();
        }
    })



 })
// fonction mouseover / mouseout
 for (var z = 0; z < 7; z++) {
     colAct[z].addEventListener('mouseover', function (event) {
         if (playerActive === 1) {
             this.style.backgroundColor = 'yellow';
         } else if (playerActive === 2) {
             this.style.backgroundColor = 'red';

         }

     });
 }
 for (var z = 0; z < 7; z++) {
     colAct[z].addEventListener('mouseout', function (event) {
         if (playerActive === 1) {
             this.style.backgroundColor = 'transparent';
         } else if (playerActive === 2) {
             this.style.backgroundColor = 'transparent';

         }
     });
 }
//  fonction de changement de joueurs
function tourJoueur() {
    // passage de joueur 1 à 2
    if (playerActive === 1) {
        console.log('joueur 1');
        console.log('tour :', 42-compteur);
        zoneGris1.style.opacity = "1";
        zoneGris2.style.opacity = "0.5";
        color = 'j';
        colorActive = 'yellow';
        // passage de joueur 2 à 1
    } else if (playerActive === 2) {
        console.log('joueur 2');
        console.log('tour :', 42-compteur);
        zoneGris2.style.opacity = "1";
        zoneGris1.style.opacity = "0.5";
        color = 'r';
        // affichage du compteur
        colorActive = 'red';
    }
    moves.innerHTML = ("Il reste " + compteur + " jetons");
};

// fonction de positionnement du jeton dans sa colonne
function place_jeton() {

    if (document.querySelector(`#x${x}y${i}`).color == null) {
        document.querySelector(`#x${x}y${i}`).color = color;
        document.querySelector(`#x${x}y${i}`).style.backgroundColor = colorActive;
        y = i;
        compteur--;
        verifGg();
    } else {
        i++;
        iteration2();
    }

};
// itération 2 --------------------------------------------------------
function iteration2() {
    if (document.querySelector(`#x${x}y${i}`).color == null) {
        document.querySelector(`#x${x}y${i}`).color = color;
        document.querySelector(`#x${x}y${i}`).style.backgroundColor = colorActive;
        y = i;
        compteur--;
        verifGg();
    } else {
        i++;
        iteration3();
    }

};
// itération 3 --------------------------------------------------------
function iteration3() {
    if (document.querySelector(`#x${x}y${i}`).color == null) {
        document.querySelector(`#x${x}y${i}`).color = color;
        document.querySelector(`#x${x}y${i}`).style.backgroundColor = colorActive;
        y = i;
        compteur--;
        verifGg();

    } else {
        i++;
        iteration4();
    }

};
// itération 4 --------------------------------------------------------
function iteration4() {
    if (document.querySelector(`#x${x}y${i}`).color == null) {
        document.querySelector(`#x${x}y${i}`).color = color;
        document.querySelector(`#x${x}y${i}`).style.backgroundColor = colorActive;
        y = i;
        compteur--;
        verifGg();
    } else {
        i++;
        iteration5();
    }
};
// itération 5 --------------------------------------------------------
function iteration5() {
    if (document.querySelector(`#x${x}y${i}`).color == null) {
        document.querySelector(`#x${x}y${i}`).color = color;
        document.querySelector(`#x${x}y${i}`).style.backgroundColor = colorActive;
        y = i;
        compteur--;
        verifGg();
    } else {
        i++;
        iteration6();
    }
};
// itération 6 --------------------------------------------------------
function iteration6() {
    if (document.querySelector(`#x${x}y${i}`).color == null) {
        document.querySelector(`#x${x}y${i}`).color = color;
        document.querySelector(`#x${x}y${i}`).style.backgroundColor = colorActive;
        y = i;
        i = 0;
        compteur--;
        verifGg();
    } else if (i = 6) {
        window.alert("Colonne remplie");
        colonneRemplie();
    }
};

// fonction de vérification de victoire ------------------------------------------------------------------------------
function verifGg() {
    verifColonne();
    verifLigne();
    verifDiagonaleSlach();
    verifDiagonaleBackSlash();
    basculeJoueur();
};

//  vérification de victoire en colonne
function verifColonne() {
    let j = 0

    do {
        if (document.querySelector(`#x${x}y${j}`).color == color &&
            document.querySelector(`#x${x}y${j+1}`).color == color &&
            document.querySelector(`#x${x}y${j+2}`).color == color &&
            document.querySelector(`#x${x}y${j+3}`).color == color) {

            document.location.href = "win.html";

        }
        j++;
    } while (j < 3);
};

// vérification de victoire en ligne---------------------------------------------------------------
function verifLigne() {
    let k = 0

    do {
        if (document.querySelector(`#x${k}y${y}`).color == color &&
            document.querySelector(`#x${k+1}y${y}`).color == color &&
            document.querySelector(`#x${k+2}y${y}`).color == color &&
            document.querySelector(`#x${k+3}y${y}`).color == color) {

            document.location.href = "win.html";
        }
        k++;
    } while (k < 3);
};

// vérification de victoire en diagonale dans le sens "slash"----------------------------------------
function verifDiagonaleSlach() {

    let j = x;
    let k = y;

    // initialisation des valeurs x et y pour la vérification de la diagonale 
    if (j != 0 && k != 0) {
        while (j != 0 && k != 0) {
            j--;
            k--;
        };
    }

    // boucle de vérification de la diagonale Slash
    if (j < 4 && k < 3) {
        do {
            if (document.querySelector(`#x${j}y${k}`).color == color &&
                document.querySelector(`#x${j+1}y${k+1}`).color == color &&
                document.querySelector(`#x${j+2}y${k+2}`).color == color &&
                document.querySelector(`#x${j+3}y${k+3}`).color == color) {

                    document.location.href = "win.html";
            }
            j++;
            k++;
        } while (j < 3 && k < 3);
    }
};

// vérification en diagonale dans le sens "back slash"--------------------------------------
function verifDiagonaleBackSlash() {

    let j = x;
    let k = y;

    // initialisation des valeurs x et y pour la vérification de la diagonale 
    if (j != 0 && k != 5) {
        while (j != 0 && k != 5) {
            j--;
            k++;
        };
    }

    // boucle de vérification de la diagonale backSlash
    if (j < 4 && k > 2) {
        do {
            if (document.querySelector(`#x${j}y${k}`).color == color &&
                document.querySelector(`#x${j+1}y${k-1}`).color == color &&
                document.querySelector(`#x${j+2}y${k-2}`).color == color &&
                document.querySelector(`#x${j+3}y${k-3}`).color == color) {

                    document.location.href = "win.html";
            }
            j++;
            k--;
        } while (j < 4 && k > 2);
    }
};

//  fonction changement de joueur
function basculeJoueur() {
    // vérifie si il reste des tours
    endgame();
    //  change la valeur du joueur actif
    if (playerActive === 2) {
        playerActive = 1;
    } else if (playerActive === 1) {
        playerActive = 2;
    }
    // réinitialise le compteur de tours du place jeton
    i = 0;
    //  rappel le tour joueur
    tourJoueur();
}

function colonneRemplie() {
    // vérifie si il reste des tours
    endgame();
    //  change la valeur du joueur actif
    if (playerActive === 1) {
        playerActive = 1;
    } else if (playerActive === 2) {
        playerActive = 2;
    }
    // réinitialise le compteur de tours du place jeton
    i = 0;
    //  rappel le tour joueur
    tourJoueur();
}

//function fin de jeu
function endgame() {
    if (compteur == 0) {
        document.location.href = "tie.html"
    }
}
//function reset
myReset.addEventListener('click', (event) => {
    console.log(myReset);
    window.location.reload();
})